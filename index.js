// alert("Hello, B273!")

/*
    1. What is the term given to unorganized code that's very hard to work with
    
    Spaghetti code

    2. How are object literals written in JS?
    
    Using {} with key-value pairs

    3. What do you call the concept of organizing information and  functionality to belong to an object?
    
    OOP

    4. If studentOne has a method named enroll(), how would you invoke it?
    
    studentOne.enroll();

    5. True or False: Objects can have objects as properties.
    
    True

    6. What is the syntax in creating key-value pairs?
    
    { key1: value1 }
    
    7. True or False: A method can have no parameters and still work.
    
    True

    8. True or False: Arrays can have objects as elements.
    
    True

    9. True or False: Arrays are objects.
    
    True

    10. True or False: Objects can have arrays as properties.
    
    True
*/

// create student one
let studentOneName = 'John';
let studentOneEmail = 'john@mail.com';
let studentOneGrades = [89, 84, 78, 88];

// create student two
let studentTwoName = 'Joe';
let studentTwoEmail = 'joe@mail.com';
let studentTwoGrades = [78, 82, 79, 85];

// create student three
let studentThreeName = 'Jane';
let studentThreeEmail = 'jane@mail.com';
let studentThreeGrades = [87, 89, 91, 93];

// create student four
let studentFourName = 'Jessie';
let studentFourEmail = 'jessie@mail.com';
let studentFourGrades = [91, 89, 92, 93];

// Object literal for Student One
let studentOne = {
    name: 'John',
    email: 'john@mail.com',
    grades: [89, 84, 78, 88],
    
    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    computeAvg() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
    },
    willPass() {
        return this.computeAvg() >= 85 ? true : false;
    },
    willPassWithHonors() {
        if(this.computeAvg() >=90) {
            return true;
        } else if (this.computeAvg() >= 85 && this.computeAvg()< 90) {
            return false;
        } else {
            return undefined;
        }
    }
}

// console.log(`Student one's name is ${studentOne.name}`);
// console.log(`Student one's email is ${studentOne.email}`);
// console.log(`Student one's quarterly grade averages are ${studentOne.grades}`);

// Object literal for Student Two
let studentTwo = {
    name: 'Joe',
    email: 'joe@mail.com',
    grades: [78, 82, 79, 85],
    
    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    computeAvg() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
    },
    willPass() {
        return this.computeAvg() >= 85 ? true : false;
    },
    willPassWithHonors() {
        if(this.computeAvg() >=90) {
            return true;
        } else if (this.computeAvg() >= 85 && this.computeAvg()< 90) {
            return false;
        } else {
            return undefined;
        }
    }
}

// console.log(`Student two's email is ${studentTwo.email}`);
// console.log(`Student two's name is ${studentTwo.name}`);
// console.log(`Student two's quarterly grade averages are ${studentTwo.grades}`);

// Object literal for Student Three
let studentThree = {
    name: 'Jane',
    email: 'jane@mail.com',
    grades: [87, 89, 91, 93],
    
    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    computeAvg() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
    },
    willPass() {
        return this.computeAvg() >= 85 ? true : false;
    },
    willPassWithHonors() {
        if(this.computeAvg() >=90) {
            return true;
        } else if (this.computeAvg() >= 85 && this.computeAvg()< 90) {
            return false;
        } else {
            return undefined;
        }
    }
}

// console.log(`Student three's email is ${studentThree.email}`);
// console.log(`Student three's name is ${studentThree.name}`);
// console.log(`Student three's quarterly grade averages are ${studentThree.grades}`);

// Object literal for Student Four
let studentFour = {
    name: 'Jessie',
    email: 'jessie@mail.com',
    grades: [91, 89, 92, 93],
    
    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    computeAvg() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
    },
    willPass() {
        return this.computeAvg() >= 85 ? true : false;
    },
    willPassWithHonors() {
        if(this.computeAvg() >=90) {
            return true;
        } else if (this.computeAvg() >= 85 && this.computeAvg()< 90) {
            return false;
        } else {
            return undefined;
        }
    }
}

// console.log(`Student four's email is ${studentFour.email}`);
// console.log(`Student four's name is ${studentFour.name}`);
// console.log(`Student four's quarterly grade averages are ${studentFour.grades}`);

let classOf1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],
    countHonorStudents() {
        const count = this.students.filter(student => student.computeAvg() >=90).length;

         return count;
    },
    honorsPercentage() {
        const count = this.students.filter(student => student.computeAvg() >=90).length;

         return (count/this.students.length) * 100;
    },
    retrieveHonorStudentInfo() {
        const honorStudents = [];
        this.students.forEach(student => {
            if (student.willPassWithHonors()) {
                    honorStudents.push({
                    email: student.email,
                    averageGrade: student.computeAvg()
                });
            }
        });
        return honorStudents;
    }
}